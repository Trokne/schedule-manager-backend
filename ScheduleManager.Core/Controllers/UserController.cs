using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ScheduleManager.Core.Services;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Constants;
using ScheduleManager.Models.Database.Authorization;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Controllers
{
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ApiController<DatabaseService<User>, User>
    {
        private readonly AuthorizationService _authService;

        public UserController(DatabaseService<User> dbService, AuthorizationService authService)
            : base(dbService) =>
            _authService = authService;

        [EnableCors("CorsPolicy")]
        [HttpGet("getbytoken")]
        public async Task<ActionResult<User>> GetByTokenAsync()
        {
            var user = await _authService.GetUserByToken(HttpContext);

            if (user != null)
                return Ok(user);

            throw new HttpStatusCodeException(HttpStatusCode.BadRequest, ExceptionMessages.NotFoundToken);
        }
    }
}