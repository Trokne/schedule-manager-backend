using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Database;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Controllers
{
    [EnableCors("CorsPolicy")]
    public abstract class ApiController<TService, TModel> : ControllerBase
        where TService : IDatabaseService<TModel>
        where TModel : IDatabaseModel
    {
        protected readonly TService _dbService;

        protected ApiController(TService dbService) => _dbService = dbService;

        [HttpGet]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult<TModel[]>> Get()
        {
            var objects = (await _dbService.SelectAsync()).ToArray();
            return Ok(objects);
        }

        [HttpGet("{id}")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult<TModel>> Get(int id)
        {
            var findObject = await _dbService.GetByIdAsync(id);
            return Ok(findObject);
        }

        [HttpPost("update")]
        [EnableCors("CorsPolicy")]
        public virtual async Task<ActionResult> Update([FromBody] TModel dbObject)
        {
            var obj = await _dbService.GetByIdAsync(dbObject.Id);

            if (obj == null)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Невозможно отредактировать несуществующий объект!");

            await _dbService.UpdateAsync(dbObject);
            return Ok(dbObject);
        }


        [HttpPost("create")]
        [EnableCors("CorsPolicy")]
        public virtual async Task<ActionResult> Create([FromBody] TModel dbObject)
        {
            var obj = await _dbService.GetByIdAsync(dbObject.Id);

            if (obj != null)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Невозможно добавить уже существующий объект!");

            try
            {
                await _dbService.InsertAsync(dbObject);
                return Ok(dbObject);
            }
            catch
            {
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Невозможно добавить уже существующий объект!");
            }
        }

        [HttpDelete("{id}")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult> Delete(int id)
        {
            var objects = await _dbService.GetByIdAsync(id);
            await _dbService.DeleteAsync(objects);
            return Ok();
        }
    }
}