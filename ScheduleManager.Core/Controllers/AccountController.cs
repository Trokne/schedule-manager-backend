using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ScheduleManager.Core.Services;
using ScheduleManager.Models.Constants;
using ScheduleManager.Models.Database.Authorization;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Controllers
{
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly AuthorizationService _authorizationService;

        public AccountController(AuthorizationService authorizationService) =>
            _authorizationService = authorizationService;

        [HttpPost("login")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult<Token>> Login([FromBody] User authInfo)
        {
            PrepareUser(authInfo);

            var identity = await _authorizationService.GetIdentity(authInfo.Login, authInfo.Password);

            if (identity == null)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest, ExceptionMessages.NotFoundUser);

            var token = await _authorizationService.GenerateTokenAsync(identity);
            return Ok(token);
        }

        [HttpPost("register")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult> Register([FromBody] User authInfo)
        {
            PrepareUser(authInfo);

            await _authorizationService.Register(authInfo);
            return Ok();
        }

        private static void PrepareUser(User authInfo)
        {
            authInfo.Login = authInfo.Login.Trim();
            authInfo.Password = authInfo.Password.Trim();
        }
    }
}