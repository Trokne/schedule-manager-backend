using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Database.Groups;

namespace ScheduleManager.Core.Controllers
{
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UniversitiesController : ApiController<DatabaseService<University>, University>
    {
        public UniversitiesController(DatabaseService<University> dbService) : base(dbService) { }
    }
}