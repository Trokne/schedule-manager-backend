using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Database.Groups;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Controllers
{
    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class GroupsController : ApiController<DatabaseService<Group>, Group>
    {
        private readonly DatabaseService<University> _universityService;

        public GroupsController(DatabaseService<Group> dbService, DatabaseService<University> universityService)
            : base(dbService) =>
            _universityService = universityService;

        [HttpPost("update")]
        [EnableCors("CorsPolicy")]
        public override async Task<ActionResult> Update([FromBody] Group dbObject)
        {
            ValidateGroup(dbObject);
            return await base.Update(dbObject);
        }

        [HttpPost("create")]
        [EnableCors("CorsPolicy")]
        public override async Task<ActionResult> Create([FromBody] Group dbObject)
        {
            ValidateGroup(dbObject);
            return await base.Create(dbObject);
        }

        [HttpGet("getrecursively")]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult> GetRecursively()
        {
            var groups = await _dbService.SelectAsync();
            var recursivelyGroups = new List<dynamic>();

            foreach (var group in groups)
            {
                var university = await _universityService.FirstOrDefaultAsync(u => u.Id == group.UniversityId);
                var recursivelyGroup = new ExpandoObject() as IDictionary<string, object>;
                recursivelyGroup["Name"] = group.Name;
                recursivelyGroup["Id"] = group.Id;
                recursivelyGroup["Description"] = group.Description;
                recursivelyGroup["UniversityId"] = group.UniversityId;
                recursivelyGroup["UniversityName"] = university.Name;
                recursivelyGroups.Add(recursivelyGroup);
            }

            return Ok(recursivelyGroups);
        }

        private void ValidateGroup(Group group)
        {
            PrepareGroup(group);

            if (string.IsNullOrWhiteSpace(group.Name))
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Название группы не должно быть пустым или состоять из одних пробелов!");

            if (string.IsNullOrWhiteSpace(group.Description))
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Описание группы не должно быть пустым или состоять из одних пробелов!");

            if (group.Description.Length > 400)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Описание группы превышает допустимые 400 символов!");

            if (group.Name.Length > 30)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Название группы превышает допустимые 30 символов!");
        }

        private void PrepareGroup(Group group)
        {
            group.Name = group.Name?.Trim();
            group.Description = group.Description?.Trim();
        }
    }
}