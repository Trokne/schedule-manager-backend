using Microsoft.AspNetCore.Builder;
using ScheduleManager.Core.Services;

namespace ScheduleManager.Core.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder builder) =>
            builder.UseMiddleware<ExceptionMiddleware>();
    }
}