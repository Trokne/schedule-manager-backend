using System;
using System.Collections.Generic;
using System.Linq;

namespace ScheduleManager.Core.Extensions
{
    public static class TypeExtensions
    {
        public static IEnumerable<Type> GetInheritedTypes(this Type mainType)
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(type => mainType.IsAssignableFrom(type)
                               && !type.IsInterface
                               && !type.IsAbstract
                               && type.IsPublic
                               && type.IsClass);
        }
    }
}