using System.Collections.Generic;
using System.IO;
using System.Linq;
using ScheduleManager.Models.Database;
using ScheduleManager.Models.Database.Authorization;
using ScheduleManager.Models.Database.Groups;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace ScheduleManager.Core.Extensions
{
    public class DatabaseInitializer
    {
        private readonly string _connectionString;

        public DatabaseInitializer(string connectionString) => _connectionString = connectionString;

        public void InitializeDatabase()
        {
            using (var connection = new SQLiteConnection(_connectionString))
            {
                CreateTables(connection);
                FillTables(connection);
            }
        }

        private void CreateTables(SQLiteConnection connection)
        {
            var tables = typeof(IDatabaseModel).GetInheritedTypes();

            foreach (var table in tables) connection.CreateTable(table);
        }

        private void FillTables(SQLiteConnection connection)
        {
            var roles = connection.GetAllWithChildren<Role>(recursive: true);

            if (!roles.Any())
                CreateRoles(connection);

            var cities = connection.GetAllWithChildren<City>(recursive: true);
            if (!cities.Any())
                CreateCities(connection);

            var universities = connection.GetAllWithChildren<University>(recursive: true);
            if (!universities.Any())
                CreateUniversities(connection);
        }

        private void CreateUniversities(SQLiteConnection connection)
        {
            var names = File.ReadAllLines("assets/university.csv");
            var universities = PrepareData(names)
                .Select(name => new University {Name = name})
                .ToArray();

            connection.InsertAll(universities);
        }

        private void CreateRoles(SQLiteConnection connection)
        {
            connection.Insert(new Role {Id = (int) Role.Types.Administrator, Name = "Administrator "});
            connection.Insert(new Role {Id = (int) Role.Types.User, Name = "User"});
        }

        private void CreateCities(SQLiteConnection connection)
        {
            var names = File.ReadAllLines("assets/city.csv");
            var cities = PrepareData(names)
                .Select(name => new City {Name = name})
                .ToArray();

            connection.InsertAll(cities);
        }

        private IEnumerable<string> PrepareData(string[] data)
        {
            return data
                .Distinct()
                .Where(name => !string.IsNullOrWhiteSpace(name))
                .OrderBy(name => name);
        }
    }
}