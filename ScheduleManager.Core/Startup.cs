﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ScheduleManager.Core.Extensions;
using ScheduleManager.Core.Services;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Auth;
using ScheduleManager.Models.Database.Authorization;
using ScheduleManager.Models.Database.Groups;

namespace ScheduleManager.Core
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) => _configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            AddServices(services);
            AddAuthorization(services);
            InitializeDatabase();
            AddCors(services);
            AddMvc(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionMiddleware();

            if (!env.IsDevelopment()) app.UseHsts();

            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseMvc();
        }

        private void AddMvc(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        private static void AddServices(IServiceCollection services)
        {
            services.AddSingleton<AuthorizationService>();
            services.AddSingleton<AuthorizationSettings>();
            services.AddSingleton<DatabaseService<User>>();
            services.AddSingleton<DatabaseService<Role>>();
            services.AddSingleton<DatabaseService<Token>>();
            services.AddSingleton<DatabaseService<Group>>();
            services.AddSingleton<DatabaseService<City>>();
            services.AddSingleton<DatabaseService<University>>();
        }

        private void InitializeDatabase()
        {
            var databaseInitializer = new DatabaseInitializer(_configuration["ConnectionString"]);
            databaseInitializer.InitializeDatabase();
        }


        private void AddCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder
                            .WithOrigins("http://localhost")
                            .WithOrigins("http://www.localhost")
                            .WithOrigins("http://localhost:8080")
                            .WithOrigins("http://www.localhost:8080")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
            });
        }

        private void AddAuthorization(IServiceCollection services)
        {
            var authSettings = _configuration.GetSection("Auth").Get<AuthorizationSettings>();
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = authSettings.ValidateIssuer,
                        ValidIssuer = authSettings.Issuer,
                        ValidateAudience = authSettings.ValidateAudience,
                        ValidAudience = authSettings.Audience,
                        ValidateLifetime = authSettings.ValidateLifetime,
                        IssuerSigningKey = SecurityKeyGenerator.SecurityKey(authSettings.EncryptionKey),
                        ValidateIssuerSigningKey = authSettings.ValidateIssuerSigningKey
                    };
                });
        }
    }
}