﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ScheduleManager.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var pathToContentRoot = GetContentRootPath(args);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(pathToContentRoot)
                .AddCommandLine(args)
                .AddJsonFile("appsettings.json")
                .Build();

            var urls = new[] {$"http://*:{configuration["PortNumber"]}"};

            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => { })
                .UseUrls(urls)
                .UseContentRoot(pathToContentRoot)
                .Build()
                .Run();
        }

        private static string GetContentRootPath(string[] args)
        {
            string pathToContentRoot;

            if (!HasRunAsService(args))
            {
                pathToContentRoot = Directory.GetCurrentDirectory();
            }
            else
            {
                // путь к файлу 
                var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                // путь к каталогу проекта
                pathToContentRoot = Path.GetDirectoryName(pathToExe);
            }

            return pathToContentRoot;
        }

        private static bool HasRunAsService(IEnumerable<string> args)
            => args?.Contains("--service") == true;
    }
}