﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Services
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next) =>
            _next = next ?? throw new ArgumentNullException(nameof(next));

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (HttpStatusCodeException ex)
            {
                if (context.Response.HasStarted) throw;

                await SendResponse(context, ex);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted) throw;

                var httpStatusCodeException = new HttpStatusCodeException(HttpStatusCode.BadRequest, ex);
                await SendResponse(context, httpStatusCodeException);
            }
        }

        private async Task SendResponse(HttpContext context, HttpStatusCodeException exception)
        {
            context.Response.Clear();
            context.Response.StatusCode = exception.StatusCode;
            context.Response.ContentType = "application/json";

            var exceptionResult = new ExceptionResult(exception);
            var response = JsonConvert.SerializeObject(exceptionResult);
            await context.Response.WriteAsync(response);
        }
    }
}