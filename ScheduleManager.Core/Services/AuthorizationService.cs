using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Auth;
using ScheduleManager.Models.Constants;
using ScheduleManager.Models.Database.Authorization;
using ScheduleManager.Models.Exceptions;

namespace ScheduleManager.Core.Services
{
    public class AuthorizationService
    {
        private readonly AuthorizationSettings _authSettings;
        private readonly DatabaseService<Role> _roleService;
        private readonly DatabaseService<Token> _tokenService;
        private readonly DatabaseService<User> _userService;

        public AuthorizationService(IConfiguration configuration,
            DatabaseService<User> userService,
            DatabaseService<Token> tokenService,
            DatabaseService<Role> roleService)
        {
            _authSettings = configuration.GetSection("Auth").Get<AuthorizationSettings>();
            _userService = userService;
            _tokenService = tokenService;
            _roleService = roleService;
        }

        public async Task<Token> GenerateTokenAsync(ClaimsIdentity identity)
        {
            var endTime = DateTime.Now.Add(TimeSpan.FromMinutes(_authSettings.LifeTimeInMinutes));

            var content = GenerateJwtToken(identity, endTime);
            var user = await _userService.FirstOrDefaultAsync(u => u.Login == identity.Name);

            var token = new Token
            {
                Content = content,
                EndTime = endTime
            };

            await _tokenService.InsertAsync(token);
            await RemoveTokenFromUserAsync(user);
            user.TokenId = token.Id;
            await _userService.UpdateAsync(user);

            return token;
        }

        public async Task<ClaimsIdentity> GetIdentity(string login, string password)
        {
            CheckUser(login, password);

            var user = await _userService.FirstOrDefaultAsync(x => x.Login == login && x.Password == password);

            if (user == null) return null;

            var role = await _roleService.FirstOrDefaultAsync(r => r.Id == user.RoleId);

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role.Name)
            };

            var claimsIdentity = new ClaimsIdentity(claims,
                "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }

        public async Task<User> GetUserByToken(HttpContext context)
        {
            var tokenContent = context.Request.Headers["Authorization"]
                .ToString()
                .Split(' ')[1];

            var token = await _tokenService.FirstOrDefaultAsync(t => t.Content == tokenContent);

            if (token == null || token.EndTime <= DateTime.Now)
                return null;

            var user = await _userService.FirstOrDefaultAsync(u => u.TokenId == token.Id);

            if (user != null)
            {
                if (token.EndTime >= DateTime.Now)
                {
                    user.Password = null;
                    return user;
                }

                user.TokenId = null;
                await _tokenService.DeleteAsync(token);
                return null;
            }

            await _tokenService.DeleteAsync(token);
            return null;
        }

        public async Task Register(User authInfo)
        {
            CheckUser(authInfo.Login, authInfo.Password);

            var existingUser = await _userService.FirstOrDefaultAsync(u => u.Login == authInfo.Login);

            if (existingUser != null)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest, ExceptionMessages.AlreadyExistsUser);

            authInfo.RoleId = (int) Role.Types.User;
            await _userService.InsertAsync(authInfo);
        }

        private string GenerateJwtToken(ClaimsIdentity identity, DateTime endTime)
        {
            var jwt = new JwtSecurityToken(
                _authSettings.Issuer,
                _authSettings.Audience,
                notBefore: DateTime.Now,
                claims: identity.Claims,
                expires: endTime,
                signingCredentials: new SigningCredentials(
                    SecurityKeyGenerator.SecurityKey(_authSettings.EncryptionKey),
                    SecurityAlgorithms.HmacSha256));

            var content = new JwtSecurityTokenHandler().WriteToken(jwt);
            return content;
        }

        private async Task RemoveTokenFromUserAsync(User user)
        {
            if (user.TokenId == null) return;

            var token = await _tokenService.FirstOrDefaultAsync(t => t.Id == user.TokenId);

            if (token == null)
            {
                user.TokenId = null;
                return;
            }

            await _tokenService.DeleteAsync(token);
        }

        private void CheckUser(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Логин пользователя не должен быть пустым или состоять из одних пробелов!");

            if (string.IsNullOrWhiteSpace(password))
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Пароль пользователя не должен быть пустым или состоять из одних пробелов!");

            if (login.Length > 30)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Логин пользователя превышает допустимую длину в 30 символов!");

            if (password.Length > 30)
                throw new HttpStatusCodeException(HttpStatusCode.BadRequest,
                    "Пароль пользователя превышает допустимую длину в 30 символов!");
        }
    }
}