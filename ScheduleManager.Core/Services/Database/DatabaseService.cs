using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ScheduleManager.Models.Database;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;

namespace ScheduleManager.Core.Services.Database
{
    public class DatabaseService<T> : IDatabaseService<T> where T : IDatabaseModel, new()
    {
        private readonly string _connectionString;

        public DatabaseService([FromServices] IConfiguration configuration) =>
            _connectionString = configuration["ConnectionString"];

        public DatabaseService(string connectionString) => _connectionString = connectionString;

        private SQLiteAsyncConnection Connection => new SQLiteAsyncConnection(_connectionString);

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return (await Connection.GetAllWithChildrenAsync<T>(recursive: true))
                .Where(x => x.Id == id)
                .ToList()
                .FirstOrDefault();
        }

        public async Task<List<T>> SelectAsync() =>
            (await Connection.GetAllWithChildrenAsync<T>(recursive: true))
            .ToList();

        public async Task<List<T>> SelectAsync(Func<T, bool> query) =>
            (await Connection.GetAllWithChildrenAsync<T>(recursive: true))
            .Where(query)
            .ToList();

        public async Task<T> FirstOrDefaultAsync(Func<T, bool> query) =>
            (await Connection.GetAllWithChildrenAsync<T>(recursive: true))
            .Where(query)
            .ToList()
            .FirstOrDefault();

        public async Task<T> FirstOrDefaultAsync() =>
            (await Connection.GetAllWithChildrenAsync<T>(recursive: true))
            .ToList()
            .FirstOrDefault();

        public virtual async Task<T> InsertAsync(T obj)
        {
            await Connection.InsertAsync(obj);
            return obj;
        }

        public virtual async Task<bool> UpdateAsync(T obj) => await Connection.UpdateAsync(obj) >= 0;

        public async Task<bool> DeleteAsync(T obj) => await Connection.DeleteAsync(obj) >= 0;
    }
}