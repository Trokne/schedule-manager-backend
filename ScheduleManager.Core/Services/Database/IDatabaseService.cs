using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScheduleManager.Core.Services.Database
{
    public interface IDatabaseService<T>
    {
        Task<T> GetByIdAsync(int id);
        Task<List<T>> SelectAsync();
        Task<List<T>> SelectAsync(Func<T, bool> query);
        Task<T> FirstOrDefaultAsync(Func<T, bool> query);
        Task<T> FirstOrDefaultAsync();
        Task<T> InsertAsync(T obj);
        Task<bool> UpdateAsync(T obj);
        Task<bool> DeleteAsync(T obj);
    }
}