namespace ScheduleManager.Models.Constants
{
    public static class ExceptionMessages
    {
        public const string NotFoundUser = "Ошибка в имени пользователя или пароле!";
        public const string NotFoundToken = "Пользователь с таким токеном не найден!";
        public const string AlreadyExistsUser = "Пользователь с таким именем уже существует!";
    }
}