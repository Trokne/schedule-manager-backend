using Newtonsoft.Json;

namespace ScheduleManager.Models.Exceptions
{
    public class ExceptionResult
    {
        public ExceptionResult(HttpStatusCodeException ex)
        {
            Message = ex.Message;
            Source = ex.Source;
            InnerMessage = ex.InnerException?.Message;
            StackTrace = ex.CallStack ?? ex.StackTrace;
        }

        [JsonProperty] public string Message { get; set; }

        [JsonProperty] public string InnerMessage { get; set; }

        [JsonProperty] public string Source { get; set; }

        [JsonProperty] public string StackTrace { get; set; }
    }
}