using System;
using System.Net;

namespace ScheduleManager.Models.Exceptions
{
    public class HttpStatusCodeException : ApplicationException
    {
        public HttpStatusCodeException(HttpStatusCode statusCode, string message)
            : base(message) =>
            StatusCode = (int) statusCode;

        public HttpStatusCodeException(HttpStatusCode statusCode, Exception ex)
            : base(ex.Message, ex.InnerException)
        {
            CallStack = string.IsNullOrWhiteSpace(ex.StackTrace) ? null : ex.StackTrace;
            StatusCode = (int) statusCode;
        }

        public int StatusCode { get; }
        public string CallStack { get; }
    }
}