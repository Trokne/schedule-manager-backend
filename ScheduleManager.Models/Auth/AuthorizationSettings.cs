using Newtonsoft.Json;

namespace ScheduleManager.Models.Auth
{
    public class AuthorizationSettings
    {
        [JsonProperty] public string Issuer { get; set; }

        [JsonProperty] public string Audience { get; set; }

        [JsonProperty] public double LifeTimeInMinutes { get; set; }

        [JsonProperty] public string EncryptionKey { get; set; }

        [JsonProperty] public bool ValidateIssuer { get; set; }

        [JsonProperty] public bool ValidateAudience { get; set; }

        [JsonProperty] public bool ValidateLifetime { get; set; }

        [JsonProperty] public bool ValidateIssuerSigningKey { get; set; }
    }
}