using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace ScheduleManager.Models.Auth
{
    public static class SecurityKeyGenerator
    {
        public static SymmetricSecurityKey SecurityKey(string encryptionKey) =>
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(encryptionKey));
    }
}