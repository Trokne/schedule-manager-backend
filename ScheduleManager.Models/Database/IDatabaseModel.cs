namespace ScheduleManager.Models.Database
{
    public interface IDatabaseModel
    {
        int Id { get; set; }
    }
}