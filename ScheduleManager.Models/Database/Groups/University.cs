using SQLite;

namespace ScheduleManager.Models.Database.Groups
{
    public class University : IDatabaseModel
    {
        public string Name { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}