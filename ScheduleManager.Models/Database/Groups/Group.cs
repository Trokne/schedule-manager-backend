using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ScheduleManager.Models.Database.Groups
{
    public class Group : IDatabaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey(typeof(University))] public int UniversityId { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}