using SQLite;

namespace ScheduleManager.Models.Database.Groups
{
    public class City : IDatabaseModel
    {
        [Unique] public string Name { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}