using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ScheduleManager.Models.Database.Authorization
{
    public class User : IDatabaseModel
    {
        [Unique] public string Login { get; set; }

        public string Password { get; set; }

        [ForeignKey(typeof(Role))] public int RoleId { get; set; }

        [ForeignKey(typeof(Token))] public int? TokenId { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}