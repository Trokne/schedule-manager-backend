using System;
using SQLite;

namespace ScheduleManager.Models.Database.Authorization
{
    public class Token : IDatabaseModel
    {
        [Unique] public string Content { get; set; }

        public DateTime EndTime { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}