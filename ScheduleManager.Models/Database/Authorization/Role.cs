using SQLite;

namespace ScheduleManager.Models.Database.Authorization
{
    public class Role : IDatabaseModel
    {
        public enum Types
        {
            Administrator = 1,
            User = 2
        }

        [Unique] public string Name { get; set; }

        [PrimaryKey] [AutoIncrement] public int Id { get; set; }
    }
}