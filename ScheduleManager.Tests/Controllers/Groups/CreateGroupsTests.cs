using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ScheduleManager.Core.Controllers;
using ScheduleManager.Core.Services.Database;
using ScheduleManager.Models.Database.Groups;
using ScheduleManager.Tests.Asserts;
using Xunit;

namespace ScheduleManager.Tests.Controllers.Groups
{
    public class GroupsControllerTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Create_SingleGroup_NullOrEmptyName(string name)
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);
            var passedGroup = GenerateGroupWithId();
            passedGroup.Name = name;

            var groupCreation = new Func<Task<ActionResult>>(async () => await groupController.Create(passedGroup));

            await ThrowAssert.ThrowAsync("Название группы не должно быть пустым или состоять из одних пробелов!",
                groupCreation);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Create_SingleGroup_NullOrEmptyDescription(string description)
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);
            var passedGroup = GenerateGroupWithId();
            passedGroup.Description = description;

            var groupCreation = new Func<Task<ActionResult>>(async () => await groupController.Create(passedGroup));

            await ThrowAssert.ThrowAsync("Описание группы не должно быть пустым или состоять из одних пробелов!",
                groupCreation);
        }

        private Group GenerateGroupWithId()
        {
            var group = GenerateGroupWithoutId();
            group.Id = 1;
            return group;
        }

        private Group GenerateGroupWithoutId() =>
            new Group
            {
                Description = "Описание",
                Name = "Группа 1",
                UniversityId = 1
            };

        private University GenerateUniversity() =>
            new University
            {
                Id = 1,
                Name = "Университет 1"
            };

        [Fact]
        public async Task Create_SingleGroup_AlreadyExists()
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateGroupWithId);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);

            var groupCreation =
                new Func<Task<ActionResult>>(async () => await groupController.Create(GenerateGroupWithId()));

            await ThrowAssert.ThrowAsync("Невозможно добавить уже существующий объект!", groupCreation);
        }

        [Fact]
        public async Task Create_SingleGroup_Correct()
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);

            var okResult = await groupController.Create(GenerateGroupWithId());
            var group = (Group) ((OkObjectResult) okResult).Value;

            Assert.Equal(1, group.Id);
        }

        [Fact]
        public async Task Create_SingleGroup_ToLongDescription()
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);
            var passedGroup = GenerateGroupWithId();

            for (var i = 0; i <= 400; i++)
                passedGroup.Description += "1";

            var groupCreation = new Func<Task<ActionResult>>(async () => await groupController.Create(passedGroup));

            await ThrowAssert.ThrowAsync("Описание группы превышает допустимые 400 символов!", groupCreation);
        }

        [Fact]
        public async Task Create_SingleGroup_ToLongName()
        {
            var mockGroupService = new Mock<DatabaseService<Group>>("");
            var mockUniversityService = new Mock<DatabaseService<University>>("");
            mockUniversityService.Setup(service => service.GetByIdAsync(1)).ReturnsAsync(GenerateUniversity);
            mockGroupService.Setup(service => service.InsertAsync(It.IsAny<Group>())).ReturnsAsync(GenerateGroupWithId);
            var groupController = new GroupsController(mockGroupService.Object, mockUniversityService.Object);
            var passedGroup = GenerateGroupWithId();

            for (var i = 0; i <= 31; i++)
                passedGroup.Name += "1";

            var groupCreation = new Func<Task<ActionResult>>(async () => await groupController.Create(passedGroup));

            await ThrowAssert.ThrowAsync("Название группы превышает допустимые 30 символов!", groupCreation);
        }
    }
}