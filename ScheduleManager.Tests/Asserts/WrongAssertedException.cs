using System;

namespace ScheduleManager.Tests
{
    [Serializable]
    public class WrongAssertedException : Exception
    {
        public WrongAssertedException()
            : base("Ожидалось исключение, но оно не произошло!") { }

        public WrongAssertedException(string expectedException)
            : base($"Ожидалось исключение \"{expectedException}\", но оно не произошло!") { }
    }
}