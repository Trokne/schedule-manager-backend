using System;
using System.Threading.Tasks;
using Xunit;

namespace ScheduleManager.Tests.Asserts
{
    public class ThrowAssert : Assert
    {
        public static void Throw(string expectedException, Action action)
        {
            try
            {
                action();
            }
            catch (Exception exception)
            {
                AssertExceptions(expectedException, exception.Message);
                return;
            }

            throw new WrongAssertedException(expectedException);
        }

        public static async Task ThrowAsync(string expectedException, Func<Task> asyncAction)
        {
            try
            {
                await asyncAction();
            }
            catch (Exception exception)
            {
                AssertExceptions(expectedException, exception.Message);
                return;
            }

            throw new WrongAssertedException(expectedException);
        }

        private static void AssertExceptions(string expectedException, string passedException)
        {
            True(expectedException == passedException,
                "Текст исключения не совпадает с ожидаемым!" +
                $"\n\nОжидаемый результат: {expectedException}," +
                $"\n\nПолученный результат: {passedException}\n");
        }
    }
}